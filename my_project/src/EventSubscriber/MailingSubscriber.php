<?php

namespace App\EventSubscriber;

use App\Entity\User;
use App\Event\ContsRequestEvent;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class MailingSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly MailerInterface $mailer
    ){}

    public function onContsRequestEvent(ContsRequestEvent $event): void
    {
        $data = $event->data;

        $email = (new TemplatedEmail())
            ->to($data->service)
            ->subject('Demande de contact')    
            ->from($data->email)
            ->htmlTemplate('emails/contact.html.twig')
            ->context(['data' => $data]);

        $this->mailer->send($email);

    }

    public function onLogin(InteractiveLoginEvent $event){
        $user = $event->getAuthenticationToken()->getUser();

        if (!$user instanceof User) {
            return;
        }

        $email = $user->getEmail();

        $email = (new TemplatedEmail())
            ->to($email)
            ->subject('Connection')    
            ->from('Support@demo.fr')
            ->text('Vous etes connecté');

        $this->mailer->send($email);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ContsRequestEvent::class => 'onContsRequestEvent',
            InteractiveLoginEvent::class => 'onLogin'
        ];
    }
}
