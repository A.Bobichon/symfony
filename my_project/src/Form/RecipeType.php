<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Recipe;
use App\Service\FormListenerFactory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\PostSubmitEvent;
use Symfony\Component\Form\Event\PreSubmitEvent;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Sequentially;

class RecipeType extends AbstractType
{
    public function __construct(
        private FormListenerFactory $factory
    ){}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class,  [
                'label'=> 'Titre',
            ])
            ->add('slug', TextType::class, [
                'constraints' => new Sequentially([
                    new Length(min: 10),
                    new Regex('/^[a-z0-9]+(?:-[a-z0-9]+)*$/', message: 'erreur de format!')
                ]),
                'required' => false,
            ])
            ->add('Category', CategoryAutocompleteField::class)
            ->add('content')
            ->add('duration')
            ->add('thumbnailFile', FileType::class)
            ->add('save', SubmitType::class, [
                'label' => 'send'
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, $this->factory->autoslug('title'))
            ->addEventListener(FormEvents::POST_SUBMIT, $this->factory->timestamp());
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Recipe::class,
            'validation_groups' => ['Default', 'Extra']
        ]);
    }
}
