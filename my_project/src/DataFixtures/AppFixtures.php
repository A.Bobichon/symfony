<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class AppFixtures extends Fixture
{
    public const ADMIN = 'ADMIN_USER';

    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher,
    ){}

    private function createUser(
        array $roles = ['ROLE_ADMIN'],
        string $username = 'admin',
        string $email = 'admin@doe.fr',
        string $password = 'admin',
        string $apiToken = 'admin_token'
    ): User{
        
        $user = new User();

        $user->setRoles($roles)
            ->setUsername($username)
            ->setEmail($email)
            ->setPassword($this->passwordHasher->hashPassword($user,$password))
            ->setApiToken($apiToken);
        
        return $user;
    }

    public function load(ObjectManager $manager): void
    {
        /** ADMIN */
        $user = $this->createUser();
        $manager->persist($user);
        $this->addReference(self::ADMIN, $user);

        /** POPULATE */
        for ($i = 0; $i < 10; $i++) {
            $user = $this->createUser(
                ['ROLE_USER'],
                "user$i",
                "user$i@doe.fr",
                0000,
                "user$i"
            );

            $manager->persist($user);
            $this->addReference("USER".$i, $user);
            $manager->flush();
        }
    }
}
