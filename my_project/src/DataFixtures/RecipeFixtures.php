<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Recipe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use FakerRestaurant\Provider\fr_FR\Restaurant;
use Symfony\Component\String\Slugger\SluggerInterface;

class RecipeFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private readonly SluggerInterface $slugger
    ){}

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $faker->addProvider(new Restaurant($faker));

        $categories = ['Plat chaud', 'Dessert', 'Entrée', 'Gouté'];

        foreach ($categories as $c) {

            $slug = $this->slugger->slug($c);

           $category = (new Category)
                ->setName($c)
                ->setSlug($slug)
                ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()))
                ->setUpdatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()));
            
            $manager->persist($category);
            $this->addReference($c, $category);
        }

        for ($i = 0; $i < 10; $i++) {

            $title = $faker->foodName();

            $recipe = (new Recipe())
                    ->setTitle($title)
                    ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()))
                    ->setUpdatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()))
                    ->setSlug($this->slugger->slug($title))
                    ->setContent($faker->paragraphs(10, true))
                    ->setDuration($faker->numberBetween(2,60))
                    ->setCategory($this->getReference($faker->randomElement($categories)))
                    ->setUser($this->getReference('USER' . $faker->numberBetween(0,9)))
                    ->setThumbnail('');

            $manager->persist($recipe);
        }

        $manager->flush();
    }

    public function getDependencies(){
        return [AppFixtures::class];
    }
}
