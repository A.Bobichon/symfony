<?php

namespace App\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

Class UserController extends AbstractController
{
    #[Route(path:'/api/me', name:'')]
    #[IsGranted('ROLE_USER')]
    public function me(){

        return $this->json(
            $this->getUser()
        );
    }
}

