<?php

namespace App\Controller\admin;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route("/admin/category", name:"admin.category.")]
#[IsGranted('ROLE_USER')]
class CategorieController extends AbstractController {
    
    #[Route("/", name:"index")]
    public function index(CategoryRepository $rep){

        return $this->render("admin/category/index.html.twig", [
            "categories"=> $rep->findAllWhitCount(),
        ]);
    }

    #[Route('/create', name:'create')]
    public function create(Request $request, EntityManagerInterface $em){
        
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em->persist($category);
            $em->flush();
            $this->addFlash('success','Categorie crée!');
            return $this->redirect('admin.category.index');
        }

        return $this->render('admin/category/create.html.twig', [ 'form' => $form ]);
    }

    #[Route('/{id}', name:'edit', requirements: ['id'=> Requirement::DIGITS], methods: ['POST', 'GET'])]
    public function edit(Request $request, EntityManagerInterface $em, Category $category){

        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em->flush();
            $this->addFlash('success','Categorie mis à jour!');
            return $this->redirect('admin.category.index');
        }

        return $this->render('admin/category/edit.html.twig', 
            [ 
                'form' => $form, 
                'cat'=> $category
            ]
        );
    }

    #[Route('/{id}', name:'delete', requirements: ['id'=> Requirement::DIGITS], methods: ['DELETE'])]
    public function remove(EntityManagerInterface $em, Category $category){
        
        $em->remove($category);
        $em->flush();
        $this->addFlash('success','Categorie supprimer!');
        return $this->redirect('admin.category.index');
    }
}