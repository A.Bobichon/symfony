<?php

namespace App\Controller\admin;

use App\Demo;
use App\Entity\Recipe;
use App\Form\RecipeType;
use App\Repository\RecipeRepository;
use App\Security\Voter\RecipeVoter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\UX\Turbo\TurboBundle;

#[Route('/admin/recipe', name:'admin.recipe.')]
class RecipeController extends AbstractController
{

    public function __construct(
        private RecipeRepository $repository,
    ){

    }

    #[Route('/', name: 'index')]
    #[IsGranted(RecipeVoter::LIST)]
    public function index(
        Request $req, 
        EntityManagerInterface $em,
        Security $security
    ): Response
    {
        $page = $req->query->getInt('page', 1);
        $userId = $security->getUser()->getId();
        $canListAll = $security->isGranted(RecipeVoter::LIST_ALL);

        $repository = $em->getRepository(Recipe::class);
        $recipes = $repository->paginationsRecipe($page, $canListAll ? null : $userId);

        return $this->render('admin/recipe/index.html.twig',
            [
                'recipes'=> $recipes,
            ]
        );
    }

    #[Route('/{slug}-{id}', name: 'show', requirements: ['id' => '\d+', 'slug' => '[a-z0-9- ]+'])]
    #[IsGranted(RecipeVoter::EDIT, subject:'recipe')]
    public function show(Request $req, string $slug, int $id): Response
    {

        $recipe = $this->repository->find($id);
        if($recipe->getSlug() !== $slug) {
            return $this->redirectToRoute('admin.recipe.show', [
                'slug'=> $recipe->getSlug(), 
                'id' => $recipe->getId() 
            ]);
        }

        return $this->render( 'admin/recipe/show.html.twig',
            [
                'recipe'=> $recipe
            ]
        );
    }

    #[Route('/{id}/edit', name:'edit', requirements: ['id'=> Requirement::DIGITS])]
    public function edit(Recipe $recipe, Request $request, EntityManagerInterface $em){

        $form_recipe = $this->createForm(RecipeType::class, $recipe);
        $form_recipe->handleRequest($request);

        if( $form_recipe->isSubmitted() && $form_recipe->isValid()){
            $em->flush();
            $this->addFlash('success', 'Modification reussi!');
            return $this->redirectToRoute('admin.recipe.index');
        }

        return $this->render('admin/recipe/edit.html.twig', [
            'recipe' => $recipe,
            'form_recipe' => $form_recipe
        ]);
    }

    #[Route('/create', name:'create')]
    #[IsGranted(RecipeVoter::CREATE)]
    public function create(Request $request, EntityManagerInterface $em){

        $recipe = new Recipe();

        $form_create = $this->createForm(RecipeType::class, $recipe);
        $form_create->handleRequest($request);

        if($form_create->isSubmitted() && $form_create->isValid()){

            $em->persist($recipe);
            $em->flush();
            $this->addFlash('success','Recipe create');

            return $this->redirectToRoute('admin.recipe.index');
        }

        return $this->render('admin/recipe/create.html.twig', [
            'form_create' => $form_create
        ]);
    }

    #[Route('/{id}/delete', name:'delete', methods: ['DELETE'],  requirements: ['id'=> Requirement::DIGITS])]
    public function remove(Request $request, Recipe $recipe, EntityManagerInterface $em){
        
        $recipeId = $recipe->getId();
        $message = 'Suppretion réussi!';
        $em->remove($recipe);
        $em->flush();

        if($request->getPreferredFormat() == TurboBundle::STREAM_FORMAT){
            $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
            return $this->render('admin/recipe/delete.html.twig', ['recipeId' => $recipeId, 'message' => $message]);
        }

        $this->addFlash('success', $message);
        return $this->redirectToRoute('admin.recipe.index');
    }

}
