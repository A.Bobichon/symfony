<?php

namespace App\Controller;

use App\DTO\ContactDTO;
use App\Event\ContsRequestEvent;
use App\Form\ContactType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Attribute\Route;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'contact')]
    public function contact(Request $req, MailerInterface $mailer, EventDispatcherInterface $dispatcher): Response
    {
        $data = new ContactDTO();

        $form = $this->createForm(ContactType::class, $data);
        $form->handleRequest($req);

        if($form->isSubmitted() && $form->isValid()){

            $dispatcher->dispatch(new ContsRequestEvent($data));

            try {
                $dispatcher->dispatch(new ContsRequestEvent($data));
                $this->addFlash('success','Votre email a bien été envoyé!');
            } catch (\Exception $e) {
                $this->addFlash('error', 'Impossible d\'envoyer votre email!');
            }

            /*
            try {

                $email = (new TemplatedEmail())
                    ->to($data->service)
                    ->subject('Demande de contact')    
                    ->from($data->email)
                    ->htmlTemplate('emails/contact.html.twig')
                    ->context(['data' => $data]);

                $mailer->send($email);
                $this->addFlash('success', 'le mail est envoyé!!!');
                return $this->redirectToRoute('contact');

            } catch (\Exception $e) {
                $this->addFlash('danger', $e->getMessage());
            }
            */
        }

        return $this->render('contact/contact.html.twig', [
            'form_contact' => $form,
        ]);
    }
}
