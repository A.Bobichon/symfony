<?php

namespace App\Service;

use Symfony\Component\Form\Event\PostSubmitEvent;
use Symfony\Component\Form\Event\PreSubmitEvent;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\String\Slugger\SluggerInterface;

class FormListenerFactory{

    public function __construct(
        private SluggerInterface $sluggerInterface,
    ){}

    public function autoSlug(string $field): callable{
        return function(PreSubmitEvent $event) use ($field){
            $datas = $event->getData();

            if(empty($datas['slug'])){
                $slugger =  $this->sluggerInterface;
                $datas['slug'] = strtolower($slugger->slug($datas[$field]));
                $event->setData($datas);
            }
        };
    }

    public function timestamp(): callable{
        return function(PostSubmitEvent $event){
            $datas = $event->getData();
            $datas->setCreatedAt( new \DateTimeImmutable());

            if(!$datas->getId()){
                $datas->setUpdatedAt( new \DateTimeImmutable());
            }   
        };
    }
}